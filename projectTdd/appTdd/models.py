# Create your models here.
from django.db import models
from django.utils import timezone

class TddModel(models.Model):
    my_status = models.CharField(max_length=300)
    my_time = models.DateTimeField(auto_now_add=True)
