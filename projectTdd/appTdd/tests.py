from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import TddModel
from .forms import Tdd_Form

# Create your tests here.

class Lab6UnitTest(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story6_contain_string(self):
        request= HttpRequest()
        response= index(request)
        string = response.content.decode("utf8")
        self.assertIn("Hello, Apa Kabar?", string)

    def test_story6_using_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, "tdd.html")

