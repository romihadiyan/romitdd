from django import forms

class Tdd_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    my_status = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs))
    
